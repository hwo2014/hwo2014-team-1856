# this is file intended for CI - put here final version or changes

require 'json'
require 'socket'

require_relative 'code/car/car'
require_relative 'code/bots/noob_bot'
require_relative 'code/misc/tcp_serve'

server_host = ARGV[0]
server_port = ARGV[1]
bot_name = ARGV[2]
bot_key = ARGV[3]

version = 'v0.3'

puts "I'm #{bot_name}, of version #{version} for CI and connect to #{server_host}:#{server_port}"

bot = FirstBot.new 1, 0.4, 0.5, 0.9

TcpServe.new(bot, nil, server_host, server_port, bot_name, bot_key)