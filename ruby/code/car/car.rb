class Car
  attr_reader :curr_piece_id, :speed, :curr_pos, :angle, :curr_lane, :next_lane
  attr_accessor :name, :color

  def initialize(name, color)
    @name = name
    @color = color
    @curr_piece_id = @angle = @prev_pos = @curr_pos = 0
  end

  def parse_car_positions(msg, curr_piece)
    car_data = our_car_data(msg)

    @angle = car_data['angle']
    @lap = car_data['lap']

    @curr_pos = car_data['piecePosition']['inPieceDistance']
    @curr_piece_id = car_data['piecePosition']['pieceIndex']
    @curr_lane = car_data['piecePosition']['lane']['startLaneIndex']
    @next_lane = car_data['piecePosition']['lane']['endLaneIndex']

    if @curr_pos < @prev_pos
      @speed = (@curr_pos + (curr_piece.length - @prev_pos))
    else
      @speed = (@curr_pos - @prev_pos)
    end

    @prev_pos = @curr_pos
  end

  private

  def our_car_data(msg)
    car_data = nil
    msg['data'].each do |arr|
      car_data = arr if arr['id']['name'] == @name
    end
    car_data
  end
end
