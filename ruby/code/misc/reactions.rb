module Reactions
  %w(carPositions gameInit join yourCar gameStart gameEnd tournamentEnd crash spawn lapFinished dnf finish error turboAvailable createRace).each do |meth|
    define_method("react_to_#{meth}") do |msg|
      if msg['gameTick']
        puts "got #{meth} at tick #{msg['gameTick']}"
      else
        puts "got #{meth}"
      end
      nil
    end
  end
end