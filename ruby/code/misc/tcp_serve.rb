class TcpServe
  def initialize(bot, track, server_host, server_port, bot_name, bot_key)
    @bot = bot
    @track = track
    tcp = TCPSocket.open(server_host, server_port)
    play(bot_name, bot_key, tcp)
  end

  private

  def play(bot_name, bot_key, tcp)
    if @track
      tcp.puts make_msg('createRace', {'botId'=>{'name' => bot_name, 'key' => bot_key}, 'trackName' => @track})
    else
      tcp.puts join_message(bot_name, bot_key)
    end
    react_to_messages_from_server tcp
  end

  def react_to_messages_from_server(tcp)
    while json = tcp.gets
      msg = JSON.parse(json)
      msg_type = msg['msgType']
      response = @bot.send("react_to_#{msg_type}", msg)
      if response
        tcp.puts message_director response
      else
        tcp.puts ping_message
      end
    end
  end

  def message_director(msg)
    case msg
      when String
        switch_message(msg)
      when Symbol
        turbo_message
      when Float
        throttle_message(msg)
      else
        ping_message
    end
  end

  def join_message(bot_name, bot_key)
    make_msg("join", {:name => bot_name, :key => bot_key})
  end

  def turbo_message
    make_msg('turbo', 'boom de yada')
  end

  def switch_message(direction)
    make_msg('switchLane', direction)
  end

  def throttle_message(throttle)
    make_msg("throttle", throttle)
  end

  def ping_message
    make_msg("ping", {})
  end

  def make_msg(msg_type, data)
    JSON.generate({:msgType => msg_type, :data => data})
  end
end