class Piece
  attr_reader :length, :radius, :angle, :switch

  def initialize(hash)
    @radius = hash['radius']
    @angle  = hash['angle']
    @switch = hash['switch']
    if @radius
      @length = 2*Math::PI*@radius*(@angle.abs/360)
    else
      @length = hash['length']
    end
  end
end