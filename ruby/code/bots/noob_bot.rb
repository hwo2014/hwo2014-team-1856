require_relative '../misc/reactions'
require_relative '../car/car'
require_relative '../piece/piece'

class FirstBot
  include Reactions
  attr_reader :our_car, :max_speed, :pieces, :min_speed, :old_message

  def initialize(max_speed, min_speed, br, a_br)
    @max_speed = max_speed
    @min_speed = min_speed
    @pieces = []
    @turn_timer = nil
    @brake_start = br
    @angle_brake = a_br
    puts "Braking will start at #{@brake_start*100}% of track's length, and until #{@angle_brake*100}% of angle"
  end

  def react_to_gameInit(msg)
    puts 'got gameInit'
    msg['data']['race']['track']['pieces'].each do |piece|
      @pieces.push Piece.new piece
      puts piece
      puts @pieces.last.length
    end

    nil
  end

  def react_to_yourCar(msg)
    puts 'got yourCar'
    data = msg['data']
    @our_car = Car.new(data['name'], data['color'])
    nil
  end

  def react_to_lapFinished(msg)
    puts "got lapFinished with time #{msg['data']['lapTime']['millis']}"
    nil
  end

  def react_to_carPositions(msg)
    if msg['gameTick']
      @our_car.parse_car_positions(msg, @pieces[@our_car.curr_piece_id])
      response = calculateMaxSpeed
      @max_speed = response if response.class == Float
      @old_message = msg
      puts "response for tcp_serve #{response} on tick #{msg['gameTick']}"
      response
    else
      puts "response is nil"
      nil
    end
  end

  def react_to_turboAvailable(msg)
    @turbo_availiable = true
    puts "got turboAvaliable with duration of #{msg['data']['turboDurationTicks']} ticks, and power factor #{msg['data']['turboFactor']}"
    nil
  end

  def react_to_gameEnd(msg)
    puts "our car finished race with time #{msg['data']['bestLaps'][0]['result']['millis']/1000}"
    nil
  end

  def react_to_crash(msg)
    puts "got crash at tick #{msg['gameTick']}"
    nil
  end

  private

  def calculateMaxSpeed

    c_p_id = @our_car.curr_piece_id
    if @pieces[c_p_id + 1]
      n_p_id = c_p_id + 1
    else
      n_p_id = 0
    end
    puts "angle-In Piece = #{@pieces[c_p_id].angle}"
    puts "Car angle = #{@our_car.angle}"
    puts "speed = #{@our_car.speed}"

    #puts "car in #{c_p_id} piece, #{@our_car.curr_pos}/#{@pieces[c_p_id].length}"
    resp =(
        if (!@pieces[c_p_id].angle && @pieces[n_p_id].angle)
          puts "Max Power on wheal NextTurn = #{@min_speed}"
          if (@our_car.curr_pos/@pieces[c_p_id].length > @brake_start)
            @max_speed*0.8
          else
            @max_speed
          end
        elsif (@pieces[c_p_id].angle && !@pieces[n_p_id].angle)
          puts "Max Power on wheal No NextTurn = #{@min_speed}"
          if (@our_car.curr_pos/@pieces[c_p_id].length > @angle_brake)
            @max_speed
          else
            @min_speed
          end
        elsif (@pieces[c_p_id].angle && @pieces[n_p_id].angle)
          puts "Max Power on wheal ON Turn and NextTurn = #{@min_speed}"
          @min_speed
        else
          puts "Max Power on wheal = #@max_speed"
          @max_speed
        end
    )

    puts "SWITCHING LANE POWAH!" if @our_car.curr_lane != @our_car.next_lane

    resp = next_angle_direction(c_p_id) if next_angle_direction(c_p_id)

    @old_direction = next_angle_direction(c_p_id)
    # 2.7620227940750404
    #(@pieces[c_p_id].angle  && (@our_car.curr_pos/@pieces[c_p_id].length < @angle_brake)) ||
    #(@pieces[c_p_id].angle && @pieces[c_p_id].angle.abs < @our_car.angle.abs)
    #if @pieces[c_p_id].angle
    #  puts "braking in piece number #{c_p_id} (angle)"
    #else
    #  puts "braking in piece number #{c_p_id} (lane)"
    #end

    #	if @pieces[@our_car.curr_piece_id].angle
    #    @turn_timer = 12 unless @turn_timer
    #    if @turn_timer > 0
    #      1 - @pieces[@our_car.curr_piece_id].angle/100
    #    else
    #      @max_speed
    #    end
    #  else
    #    @turn_timer = nil
    #			1.0
    #	end
    resp
  end

  def next_angle_direction(c_p_id)
    @pieces[c_p_id..@pieces.size].each do |piece|
      if piece.angle && piece.angle != @old_direction
        if piece.angle > 0
          return 'Right'
        else
          return 'Left'
        end
      else
        return nil
      end
    end
  end
end