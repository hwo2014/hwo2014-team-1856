require 'json'
require 'socket'

require_relative 'code/car/car'
require_relative 'code/bots/noob_bot'
require_relative 'code/misc/tcp_serve'

server_host = ARGV[0]
server_port = ARGV[1]
bot_name = ARGV[2]
bot_key = ARGV[3]

version = 'v0.3'

puts "I'm #{bot_name}, of version #{version} and connect to #{server_host}:#{server_port}"

bot = FirstBot.new 0.5, 0.4, 0.5, 0.9
# available tracks: keimola, germany, usa, france
TcpServe.new(bot, 'keimola', server_host, server_port, bot_name, bot_key)
# 1.0 -  1.6625223786985002 at 11nth tick
# 0.5 -  1.5938368788005377 at 21nth tick
# 0.25 - 1.363009173594513 at 41nth tick